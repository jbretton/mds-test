import React from "react";
import {Button, Form} from "react-bootstrap";
import {useRatingStore} from "../store/ratingStore.ts";
import {format} from "date-fns";

function FormRating() {
  const [title, setTitle] = React.useState("");
  const [note, setNote] = React.useState(0);
  const [comment, setComment] = React.useState("");
  const { addRating } = useRatingStore();

  const handleSubmit = e => {
    e.preventDefault();
    addRating({
      id: Math.floor(Math.random() * 10000),
      name: title,
      rating: note,
      comment: comment,
      date: format(new Date(), 'dd/MM/yyyy H:m:ss'),
      updatedAt: format(new Date(), 'dd/MM/yyyy H:m:ss'),
    });
    setTitle("");
    setComment("");
    setNote(0);
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Group>
        <Form.Label><b>Ajouter une note</b></Form.Label>
        <Form.Control type="text" className="input mb-2" value={title} onChange={e => setTitle(e.target.value)} placeholder="Titre" />
        <Form.Control type="number" className="input mb-2" value={note} onChange={e => setNote(parseInt(e.target.value))} placeholder="Note" />
        <Form.Control type="text" className="input" value={comment} onChange={e => setComment(e.target.value)} placeholder="Commentaire" />
      </Form.Group>
      <Button variant="primary mb-3 mt-2" type="submit">
        Enregistrer
      </Button>
    </Form>
  );
}

export default FormRating;