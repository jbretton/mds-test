import {describe, test, expect, beforeEach} from "vitest";
import {useRatingStore} from "../store/ratingStore.ts";

describe("FormRating", () => {
  beforeEach(() => {
    const { reset } = useRatingStore.getState();
    reset();
  });

  test("Test can add rating in the ratings array", () => {
    const { addRating } = useRatingStore.getState();

    addRating({
      id: 1,
      name: 'test',
      rating: 2,
      comment: 'test commentaire',
      date: '2021-01-01',
      updatedAt: '2021-01-01',
    });

    const { ratings } = useRatingStore.getState();
    expect(ratings).toMatchSnapshot();
  });

  test("Test can add multiple element in the ratings array", () => {
    const { addRating } = useRatingStore.getState();

    addRating({
      id: 1,
      name: 'test',
      rating: 2,
      comment: 'test commentaire',
      date: '2021-01-01',
      updatedAt: '2021-01-01',
    });
    addRating({
      id: 2,
      name: 'Deuxième test',
      rating: 4,
      comment: 'Ceci est un commentaire',
      date: '2021-02-02',
      updatedAt: '2021-02-02',
    });

    const { ratings } = useRatingStore.getState();
    expect(ratings).toMatchSnapshot();
  });
});
