import {Button, Form, Modal} from "react-bootstrap";
import {useRatingStore} from "../store/ratingStore.ts";
import React, {useEffect, useState} from "react";
import {format} from "date-fns";

function Rating({ key, element }) {
  const [show, setShow] = useState(false);
  const [title, setTitle] = React.useState("");
  const [note, setNote] = React.useState(0);
  const [comment, setComment] = React.useState("");
  const [updatedAt, setUpdatedAt] = React.useState("");

  useEffect(() => {
    setTitle(element.name);
    setNote(element.rating);
    setComment(element.comment);
    setUpdatedAt(element.updatedAt);
  }, [element.name, element.rating, element.comment, element.updatedAt]);

  const { deleteRating, updateRating } = useRatingStore();

  const removeRating = index => {
    deleteRating(index);
  };

  const handleShow = e => {
    e.preventDefault();
    setShow(true);
  }

  const handleClose = e => {
    e.preventDefault();
    setShow(false)
    ;}

  const handleModify = e => {
    e.preventDefault();
    updateRating({
      id: element.id,
      name: title,
      rating: note,
      comment: comment,
      date: element.date,
      updatedAt: format(new Date(), 'dd/MM/yyyy H:m:ss'),
    });
    setTitle("");
    setComment("");
    setNote(0);
    setShow(false)
  };

  return (
    <>
      <div
        className="rating"
        key={element.id}
        onClick={handleShow}
      >
        <div>
          <p>Titre</p>
          <p>{element.name}</p>
        </div>
        <div>
          <p>Note</p>
          <p>{element.rating}</p>
        </div>
        <div>
          <p>Commentaire</p>
          <p>{element.comment}</p>
        </div>
        <div>
          <p>Date de création</p>
          <p>{element.date}</p>
        </div>
        <div>
          <Button style={{marginRight: "10px"}} variant="outline-dark" onClick={() => handleShow}>
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pen"
                 viewBox="0 0 16 16">
              <path
                d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z"/>
            </svg>
          </Button>
          <Button variant="outline-dark delete" onClick={() => removeRating(element.id)}>
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash"
                 viewBox="0 0 16 16">
              <path
                d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
              <path
                d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
            </svg>
          </Button>
        </div>
      </div>

      <Modal show={show} size="xl">
        <Modal.Header>
          <Modal.Title>Note : {element.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleModify}>
            <Form.Group>
              <Form.Label><b>Titre</b></Form.Label>
              <Form.Control type="text" className="input mb-2" value={title} onChange={e => setTitle(e.target.value)} placeholder="Titre" />
              <Form.Label><b>Note</b></Form.Label>
              <Form.Control type="number" className="input mb-2" value={note} onChange={e => setNote(parseInt(e.target.value))} placeholder="Note" />
              <Form.Label><b>Commentaire</b></Form.Label>
              <Form.Control type="text" className="input mb-4" value={comment} onChange={e => setComment(e.target.value)} placeholder="Commentaire" />
              <Form.Label><b>Date de création</b></Form.Label>
              <p>{element.date}</p>
              <Form.Label><b>Dernière modification</b></Form.Label>
              <p>{updatedAt}</p>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Fermer
          </Button>
          <Button variant="success" onClick={handleModify}>
            Modifier
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default Rating;