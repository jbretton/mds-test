import {describe, test, expect, beforeEach} from "vitest";
import {useRatingStore} from "../store/ratingStore.ts";

describe("Rating", () => {
  beforeEach(() => {
    const { reset } = useRatingStore.getState();
    reset();
  });

  test("Test can add a rating and reset the store", () => {
    const { addRating, reset } = useRatingStore.getState();

    addRating({
      id: 1,
      name: 'test',
      rating: 2,
      comment: 'test commentaire',
      date: '2021-01-01',
      updatedAt: '2021-01-01',
    });
    reset();

    const { ratings } = useRatingStore.getState();
    expect(ratings).toEqual([]);
  });

  test("Test can add and update a rating in the ratings array", () => {
    const { addRating, updateRating } = useRatingStore.getState();

    addRating({
      id: 1,
      name: 'test',
      rating: 2,
      comment: 'test commentaire',
      date: '2021-01-01',
      updatedAt: '2021-01-01',
    });
    updateRating({
      id: 1,
      name: 'test modifié',
      rating: 4,
      comment: 'test commentaire',
      date: '2021-01-01',
      updatedAt: '2021-02-02',
    });

    const { ratings } = useRatingStore.getState();
    expect(ratings).toEqual([
      {
        id: 1,
        name: 'test modifié',
        rating: 4,
        comment: 'test commentaire',
        date: '2021-01-01',
        updatedAt: '2021-02-02',
      }
    ]);
  });

  test("Test can add multiple element and update one in the ratings array", () => {
    const { addRating, updateRating } = useRatingStore.getState();

    addRating({
      id: 1,
      name: 'test',
      rating: 2,
      comment: 'test commentaire',
      date: '2021-01-01',
      updatedAt: '2021-01-01',
    });
    addRating({
      id: 2,
      name: 'Deuxième test',
      rating: 4,
      comment: 'Ceci est un commentaire',
      date: '2021-02-02',
      updatedAt: '2021-02-02',
    });
    updateRating({
      id: 2,
      name: 'Deuxième test modifié',
      rating: 4,
      comment: 'Ceci est un commentaire',
      date: '2021-02-02',
      updatedAt: '2021-03-03',
    });

    const { ratings } = useRatingStore.getState();
    expect(ratings).toEqual([
      {
        id: 1,
        name: 'test',
        rating: 2,
        comment: 'test commentaire',
        date: '2021-01-01',
        updatedAt: '2021-01-01',
      },
      {
        id: 2,
        name: 'Deuxième test modifié',
        rating: 4,
        comment: 'Ceci est un commentaire',
        date: '2021-02-02',
        updatedAt: '2021-03-03',
      }
    ]);
  });

  test("Test can add rating in the ratings array and delete", () => {
    const { addRating, deleteRating } = useRatingStore.getState();

    addRating({
      id: 1,
      name: 'test',
      rating: 2,
      comment: 'test commentaire',
      date: '2021-01-01',
      updatedAt: '2021-01-01',
    });
    deleteRating(1);

    const { ratings } = useRatingStore.getState();
    expect(ratings).toEqual([]);
  });

  test("Test can add multiple rating in the ratings array and delete one", () => {
    const { addRating, deleteRating } = useRatingStore.getState();

    addRating({
      id: 1,
      name: 'test',
      rating: 2,
      comment: 'test commentaire',
      date: '2021-01-01',
      updatedAt: '2021-01-01',
    });
    addRating({
      id: 2,
      name: 'testiiii',
      rating: 4,
      comment: 'test la vie',
      date: '2021-01-01',
      updatedAt: '2021-01-01',
    });
    deleteRating(2);

    const { ratings } = useRatingStore.getState();
    expect(ratings).toEqual([
      {
        id: 1,
        name: 'test',
        rating: 2,
        comment: 'test commentaire',
        date: '2021-01-01',
        updatedAt: '2021-01-01',
      }
    ]);
  });
});
