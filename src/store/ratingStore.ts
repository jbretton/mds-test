import {create} from "zustand";

export interface RatingInt {
  id: number;
  name: string;
  rating: number;
  comment: string;
  date: string;
  updatedAt: string;
}

interface RatingStore {
  ratings: RatingInt[];
  addRating: (rating: RatingInt) => void;
  updateRating: (updatedRating: RatingInt) => void;
  deleteRating: (ratingId: number) => void;
  reset: () => void;
}

export const useRatingStore = create<RatingStore>((set) => ({
  ratings: [],
  addRating: (rating) =>
    set((state) => ({ ...state, ratings: [...state.ratings, rating] })),
  updateRating: (updatedRating) =>
    set((state) => ({
      ...state,
      ratings: state.ratings.map((rating) =>
        rating.id === updatedRating.id ? { ...updatedRating } : rating
      ),
    })),
  deleteRating: (ratingId) =>
    set((state) => ({
      ...state,
      ratings: state.ratings.filter((rating) => rating.id !== ratingId),
    })),
  reset: () => set(() => ({ratings: []})),
}));
