import "./App.css";
import { Card } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import FormRating from "./components/FormRating.tsx";
import Rating from "./components/Rating.tsx";
import {useRatingStore} from "./store/ratingStore.ts";

function App() {
	const { ratings } = useRatingStore();

	return (
		<div className="app">
			<div className="container">
				<h1 className="text-center mb-4">Liste des notes</h1>
				<FormRating />
				<div data-testid={"listing"} className={"card-list"}>
					{ratings.map((element, index) => (
						<Card
							className={"mb-2"}
							style={{ backgroundColor: element.rating < 8 ? "red" : element.rating < 10 ? "orange" : element.rating < 13 ? "yellow" : "green" }}
						>
							<Card.Body>
								<Rating
									key={index}
									element={element}
								/>
							</Card.Body>
						</Card>
					))}
				</div>
			</div>
		</div>
	);
}

export default App;