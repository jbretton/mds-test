describe('template spec', () => {
  it('Should add one rating', () => {
    cy.visit('http://localhost:5173');
    cy.get('[placeholder="Titre"]').clear('N');
    cy.get('[placeholder="Titre"]').type('Note du devoir');
    cy.get('[placeholder="Note"]').clear('1');
    cy.get('[placeholder="Note"]').type('15');
    cy.get('[placeholder="Commentaire"]').clear('B');
    cy.get('[placeholder="Commentaire"]').type('Bonne note !');
    cy.get('.btn').click();
  });

  it('Should add two ratings', () => {
    cy.visit('http://localhost:5173');
    cy.get('[placeholder="Titre"]').clear('N');
    cy.get('[placeholder="Titre"]').type('Note du devoir');
    cy.get('[placeholder="Note"]').clear('1');
    cy.get('[placeholder="Note"]').type('15');
    cy.get('[placeholder="Commentaire"]').clear('B');
    cy.get('[placeholder="Commentaire"]').type('Bonne note !');
    cy.get('.btn').click();
    cy.get('[placeholder="Titre"]').clear('D');
    cy.get('[placeholder="Titre"]').type('Deuxième note');
    cy.get('[placeholder="Note"]').clear('1');
    cy.get('[placeholder="Note"]').type('10');
    cy.get('[placeholder="Commentaire"]').clear('B');
    cy.get('[placeholder="Commentaire"]').type('Bof la note');
    cy.get('form > .btn').click();
  });

  it('Should add one rating and open/close the modal', () => {
    cy.visit('http://localhost:5173');
    cy.get('[placeholder="Titre"]').clear('N');
    cy.get('[placeholder="Titre"]').type('Note du devoir');
    cy.get('[placeholder="Note"]').clear('1');
    cy.get('[placeholder="Note"]').type('15');
    cy.get('[placeholder="Commentaire"]').clear('B');
    cy.get('[placeholder="Commentaire"]').type('Bonne note !');
    cy.get('.btn').click();
    cy.get('[style="margin-right: 10px;"] > .bi').click();
    cy.get('.btn-secondary').click();
  });

  it('Should add one rating and update the content', () => {
    cy.visit('http://localhost:5173');
    cy.get('[placeholder="Titre"]').clear('N');
    cy.get('[placeholder="Titre"]').type('Note du devoir');
    cy.get('[placeholder="Note"]').clear('1');
    cy.get('[placeholder="Note"]').type('15');
    cy.get('[placeholder="Commentaire"]').clear('B');
    cy.get('[placeholder="Commentaire"]').type('Bonne note !');
    cy.get('.btn').click();
    cy.get('[style="margin-right: 10px;"] > .bi').click();
    cy.get('.modal-body > form > div > [placeholder="Note"]').clear('16');
    cy.get('.modal-body > form > div > [placeholder="Note"]').type('19');
    cy.get('.btn-success').click();
  });

  it('Should add one rating and remove it', () => {
    cy.visit('http://localhost:5173');
    cy.get('[placeholder="Titre"]').clear('N');
    cy.get('[placeholder="Titre"]').type('Note du devoir');
    cy.get('[placeholder="Note"]').clear('1');
    cy.get('[placeholder="Note"]').type('15');
    cy.get('[placeholder="Commentaire"]').clear('B');
    cy.get('[placeholder="Commentaire"]').type('Bonne note !');
    cy.get('.btn').click();
    cy.get('.delete').click();
  });
})